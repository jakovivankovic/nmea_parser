import pynmea2
import time
import utm

read_file = open('nmea_home.txt', encoding='utf-8')
write_file = open("NMEA_to_UTM.txt", "a")


write_file.write("UTM_X,UTM_Y,Altitude,Time\n\n")
for line in read_file.readlines():
    try:
        msg = pynmea2.parse(line)
        
        if line.startswith( '$GNGGA' ) :
             if ((msg.lat) and (msg.lon)):
                 utm_temp = utm.from_latlon(float(msg.latitude), float(msg.longitude))
                 #Write to file, rounded 
                 write_file.write("%s,%s,%s,%s\n" %(round(utm_temp[0],2),round(utm_temp[1],2),msg.altitude,msg.data[0] ))
             
             

    except pynmea2.ParseError as e:
##        print('Parse error: {}'.format(e))
        continue

write_file.close()

